json.array!(@artists) do |artist|
  json.extract! artist, :id, :name, :description, :price
  json.url artist_url(artist, format: :json)
end
